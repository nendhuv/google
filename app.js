/**
 * Module dependencies.
 */

var express = require('express');
var bodyParser = require('body-parser');
var path = require('path');
var app = express();
var http = require('http');
var server = http.createServer(app);

app.set('port', process.env.PORT || 3000);
app.set('view engine', 'jade');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded());


app.use(express.static(path.join(__dirname, 'scripts')));

app.get('/', function(req,res){
	res.render('index')
})

/**
 * 404 Error Handler
 */

app.get('*', function(req, res) {
    res.json(404, {
        error: 'Oops, you are lost!'
    })
});

server.listen(app.get('port'), function() {
    console.log("✔ Express server listening on port %d in %s mode", app.get('port'), app.get('env'));
});

module.exports = app;
'use strict';
angular.module('google-signin',[])
 .directive('googlePlus' , function(){
 		return{
 			restrict: 'E',
 			transclude: true,
 			replace: true,
      template : '<div ng-click=\'$scope.login()\'>Login</div>',
      scope:{
      	clientid : '=',
      	googlescopes : '@'
      },
      controller: ['$scope', '$http','$window', function($scope, $http, $window) {
      var tokenUrl;
      $scope.getData = function(token, userinfo){
            $http.defaults.headers.common.Authorization = 'Bearer ' + token.access_token;
            $http.get(userinfo).success(function(data){
              console.log(data);
            })
      }

      }],
 			link: function(scope, element, attrs, $http, $scope){
        var userinfo = attrs.info;
        attrs.clientid= attrs.clientid+'.apps.googleusercontent.com'
        attrs.googlescopes= attrs.googlescopes.replace(/,/g, '+')
        var params ={
          clientid : attrs.clientid,
          callback: 'signinCallback',
          
          scope: attrs.googlescopes,
          height: 'standard',
          width: 'wide',
          state: ''
        }
        var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
        po.src = 'https://apis.google.com/js/client.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);

        element.bind('click', function(){
          var config = {
            'client_id': attrs.clientid,
            'cookiepolicy': 'single_host_origin',
            'scope': attrs.googlescopes
          };
          gapi.auth.authorize(config, function() {
            console.log('login complete');
            var token = gapi.auth.getToken();
            scope.getData(token,userinfo);
          });
        })
 			}
 		}
 })